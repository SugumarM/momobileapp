﻿
app.controller('GraphCtrl', ['$scope', '$location', 'graphService', '$state', 'clientService', '$rootScope','storageService',function ($scope, $location, graphService, $state, clientService, $rootScope,storageService) {

    $scope.showFeedback = $rootScope.showFeedback;
    $scope.graphTitle = "";
    $scope.graphTypeValue = $state.params.gt != null ? $state.params.gt : 3;
// alert($scope.graphTypeValue);

$scope.graphType = {
       
        graphType: $state.params.gt != null ? $state.params.gt : 3
    };

// alert(JSON.stringify($scope.graphType));

    $scope.navigateHome = function () {
        $state.go('tab.home');
    };

    // $scope.navigateFeedback = function () {
        
    //     if ($scope.graphType == 3) {
    //         $state.go('ors-feedback');
    //     }
    //     else {
    //         $state.go('srs-feedback');
    //     }
    // };

    $scope.graph = {};
    $scope.client = {};

    // we shouldn't need to call this here.... Let's find a different solution for this
    //clientService.getClient().then(function (results) {
        
    //    $scope.client = results.data;
    //    console.log(results.data);
    //    //$scope.showFeedback = $scope.client.showFeedback;

    //}, function (error) {
    //    //alert(error.data.message);
    //});
if($rootScope.isOnline)
{
    graphService.getGraph($scope.graphType).then(function (results) {

  $scope.graph = results.data;

     if ($scope.graphTypeValue == 3) {
storageService.removeorsgraphData();
storageService.addorsgraphData($scope.graph);
    //    alert("ORS");
    }
    else {
        storageService.removesrsgraphData();
        storageService.addsrsgraphData($scope.graph);
    //    alert("SRS");
    };

       
    //    alert(JSON.stringify($scope.graph));
       
            //    console.log($scope.graph);

    }, function (error) {
        //alert(error.data.message);
    });
}

else{

     if ($scope.graphTypeValue == 3) {
    var graphInfo = storageService.getorsgraphData();

      var arrayCount= graphInfo.length;
      if(arrayCount>0)

      {
$scope.graph = graphInfo[0];
$('#offlineText').html("<div style='position:fixed; width:100%; font-size: 12px; text-align:center'>Note: Since this device is currently offline - the graph will only show the most recent ORS scores that were uploaded to the MyOutcomes server. Once the device is connect to the internet - the graph will reflect the most recent scores entered.</div>");
        if(Object.keys($scope.graph).length == 0)
            {
                // alert("NUll Data");
           $('#container').html("<div style='position:fixed; width:100%; font-size: 22px; text-align:center; top:45%'>No Graph Loaded</div>");
            }
      }

      else{

         $('#container').html("<div style='position:fixed; width:100%; font-size: 22px; text-align:center; top:45%'>No Graph Loaded</div>");
      };


    //    alert("ORS");
    }
    else {
         var graphInfo = storageService.getsrsgraphData();
         var arrayCount= graphInfo.length;

         if(arrayCount>0)
         {
$scope.graph = graphInfo[0];


$('#offlineText').html("<div style='position:fixed; width:100%; font-size: 12px; text-align:center'>Note: Since this device is currently offline - the graph will only show the most recent SRS scores that were uploaded to the MyOutcomes server. Once the device is connect to the internet - the graph will reflect the most recent scores entered.</div>");

    if(Object.keys($scope.graph).length == 0)
            {
                // alert("NUll Data");
           $('#container').html("<div style='position:fixed; width:100%; font-size: 22px; text-align:center; top:45%'>No Graph Loaded</div>");
            }

         }

         else
         {
             $('#container').html("<div style='position:fixed; width:100%; font-size: 22px; text-align:center; top:45%'>No Graph Loaded</div>"); 
         };

    //    alert("SRS");
    };




};

    
}]);