'use strict';
app
.controller('SignInCtrl', ['$scope','CordovaNetwork','$rootScope' ,'$location','authService', '$state','ngAuthSettings' ,'translationService', '$ionicPopup','storageService','$filter',function ($scope,CordovaNetwork,$rootScope,$location, authService, $state, ngAuthSettings, translationService,$ionicPopup,storageService,$filter) {


 CordovaNetwork.isOnline().then(function(isConnected) {
    
if(isConnected)
{
 $rootScope.isOnline=true;
loginfunction(); 
}

else
{
$rootScope.isOnline=false;
loginfunction();
} 
  }).catch(function(err){
    console.log(err);
  });


    $scope.loginData = {
        userName: "",
        password: ""
    };
    
    $scope.region = "";
    $scope.message = "";
    
    $scope.showWarning=false;
    $scope.isSelected=false;

    // var loginInfo = authService.getLoginData();

    var userInfo = storageService.getuserData();
    var serverInfo = storageService.getserverData();
  var prevloginTime = storageService.getloginTime();


    // var prevTime = 243;
    // var dateHours = $filter('date')(new Date(), 'HH');
    // var dateMinutes = $filter('date')(new Date(), 'mm');
    // // var date = 100 + 50 * 3;
    // var date = Number(dateMinutes) + Number(dateHours) * 60;

    //  var diff = date - prevTime;

// alert(diff);
//     alert(date);
//      if(diff>720)
//      {
//          alert("User Auto Logout");
//      }


    $scope.login = function () {

        if ($scope.loginData.userName == "" || ($scope.loginData.password == ""))
        {
            $ionicPopup.alert({
                title: "Login Information",
                content: "Please enter login information!"
            })
            .then(function (result) {
                return;
    });
   
        }

        else if($scope.isSelected==false)
        {
            $ionicPopup.alert({
                title: "Please select your country!",
                content: ""
            })
        }
       
        else{      
               authService.login($scope.loginData).then(function (response) {
               storageService.removeuserData();
               storageService.removeloginTime();
               storageService.adduserData($scope.loginData);

    // var timeHours = $filter('date')(new Date(), 'HH');
    // var timeMinutes = $filter('date')(new Date(), 'mm');
    // var loginTime = Number(timeMinutes) + Number(timeHours) * 60;

var loginTime = new Date();
// var previous = new Date(2016,10,13);

// alert(loginTime);

    storageService.addloginTime(loginTime);

               console.log($scope.loginData);

               var  userId = $scope.loginData.userName;

       var claintId=userId.slice(0, 2);
  
              if(claintId!=null && claintId!="" &&  claintId.toLowerCase()=="c#")
              {
                 $state.go('tab.home');

              }
              else
              {
                 $state.go('tabprovider.clientlist');
              };
               
            },
             function (err) {
             
                     var mes = "";
                     if (err.error_description == "" || err.error_description == undefined) mes = "Please try again later!";
                     else mes = err.error_description;
                    
                     $ionicPopup.alert({
                         title: "Login Information",
                         content: mes
                     })
                .then(function (result) {
                    return;
     
                });

             });
        }
    
};
 
    $scope.setServer = function (selected) {

        $scope.isSelected=true;

        storageService.removeserverData();

        storageService.addserverData(selected);

        if (selected == "CA" || selected == "UK") {
            ngAuthSettings.setBaseUrl('http://rest.myoutcomesapp.com/');
            $scope.server=selected;
        } 

        else if (selected == "US"|| selected == "INT") {
            ngAuthSettings.setBaseUrl('http://rest.myoutcomesapp.com/');
            $scope.server=selected;
        } 

        else
        {
            $ionicPopup.alert({
                title: "Please select your country!",
                content: ""
            }); 
        }
      
    };

  $scope.popup=function(showWarning){

storageService.removeuserData();
storageService.removeserverData();

      storageService.adduserData($scope.loginData);
      storageService.addserverData($scope.server);
     

        console.log(showWarning);
        if (showWarning)
        {
            
                    $ionicPopup.alert({
                title: "Auto Login Selected",
                content: "You will be logged in automatically next time you use this device"
            })
            .then(function (result) {
                
                return;

            });
        }

    };

function loginfunction() 
{
    if (serverInfo!="" && userInfo != null && userInfo.length > 0) {

    // var prevTime = 243;
    //  var timeHours = $filter('date')(new Date(), 'HH');
    //  var timeMinutes = $filter('date')(new Date(), 'mm');
    // var currentTime = Number(timeMinutes) + Number(timeHours) * 60;

    //  var timeDiff = Number(currentTime) - Number(prevloginTime);
    var currentTime= new Date();
    var previousTime= new Date(prevloginTime);

    var timeDiff = currentTime.getTime() - previousTime.getTime();
// alert(timeDiff);
     if(timeDiff >= 43200000)
     {
        //  alert("User Auto Logout");
         authService.logOut();
         $state.go('signin');
     }

else {


        $scope.isSelected=true;

         if (serverInfo == "CA" || serverInfo == "UK") {
           
            ngAuthSettings.setBaseUrl('http://rest.myoutcomesapp.com/');
        } 
        else 
        {
            ngAuthSettings.setBaseUrl('http://rest.myoutcomesapp.com/');
        }; 

        var userInfoObject=userInfo[0];
        var  userId = userInfoObject.userName;
        var claintId=userId.slice(0, 2);
       
       if(claintId!=null && claintId!="" &&  claintId.toLowerCase()=="c#")
        {
      
            if($rootScope.isOnline)
            {
             $scope.loginData.userName = userInfoObject.userName;
            $scope.loginData.password = userInfoObject.password;

            if ($scope.loginData.userName!="" && $scope.loginData.password!=""){
                $scope.login();
            }

            }
            
            else
            {
                 $state.go('tab.home');
            };
   
         }
         else
         {

           if($rootScope.isOnline)
            {
             $scope.loginData.userName = userInfoObject.userName;
            $scope.loginData.password = userInfoObject.password;
      
            if ($scope.loginData.userName!="" && $scope.loginData.password!=""){
                $scope.login();
            }

            }
            
            else
            {
                 $state.go('tabprovider.clientlist');
            };
         };
};
    }

    else
    {
       $state.go('signin');
    };
};

}]);