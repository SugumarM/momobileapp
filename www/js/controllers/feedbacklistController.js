﻿'use strict';

 app.controller('feedbacklistCtrl', ['$state','$q','$scope','$rootScope','feedbacklistService','$ionicPopup','storageService','providersurveyService',function ($state,$q,$scope,$rootScope,feedbacklistService,$ionicPopup,storageService,providersurveyService) {


//  var CoupleID=$rootScope.coupleClientID;

// alert($scope.CoupleID);
$scope.parentID=$rootScope.parentClientID;
$scope.parentName=$rootScope.parentName;

         $scope.gotoSrs = function (client) {

             $rootScope.clientId=client.userId;
             $rootScope.languageID=client.languageId;
             $rootScope.providergraphType=2;
            //  alert(JSON.stringify(client));
  
            $rootScope.isAdultClient=client.adulT_SURVEY_TYPE;

            $rootScope.isGroupSrs=client.isGroupUser;

            $rootScope.isFromfeedbacklist=true;
            $rootScope.isFromCouplelist=false;

         $state.go('tabprovider.srs');

    };

        $scope.gotoOrs = function (client) {
      
             $rootScope.clientId=client.userId;
             $rootScope.languageID=client.languageId;
              $rootScope.providergraphType=3;
             $rootScope.isAdultClient=client.adulT_SURVEY_TYPE;
                  //  alert(JSON.stringify(client));
            $scope.isAdult=client.adulT_SURVEY_TYPE;

$rootScope.isFromfeedbacklist=true;
$rootScope.isFromCouplelist=false;

                  $state.go('tabprovider.ors');
    };

        $scope.gotoCsr = function (userId) {
       
              $rootScope.clientId=userId;
               $rootScope.providergraphType=1;
        $state.go('graph-provider');
    };

if($rootScope.isOnline)
{
          feedbacklistService.getClientlist($scope.parentID)
                .then(function (results) {
                    $scope.feedbackclients = results;
                        storageService.removefeedbackList();
                     storageService.addfeedbackList($scope.feedbackclients);
            //      alert(JSON.stringify($scope.clients));
                },function(err) {
                    var mes = "";
                     if (err.error_description == "" || err.error_description == undefined) mes = "Please try again later!";
                     else mes = err.error_description;

                     $ionicPopup.alert({
                         title: "No Response",
                         content: mes
                     })
                });
}

else
{

var feedbackData = storageService.getfeedbackList();
$scope.feedbackclients = feedbackData[0];

};   
     
     }]);