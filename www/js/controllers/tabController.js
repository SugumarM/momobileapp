app.controller('TabCtrl',['$scope', '$state', 'authService','$ionicPopup'
	, function ($scope, $state, authService, $ionicPopup) {
    $scope.navigateHome = function () {
        $state.go('tab.home');
    };

    $scope.logout=function(){
        var confirmPopup = $ionicPopup.confirm({
         title: "Logging out",
         template: "Password and username will be requested next time the application is opened"        });

           confirmPopup.then(function(res) {
             if(res) {
                authService.logOut(); 
                $state.go('signin');              
            } else {
            //    $state.go('tab.home');
             }
           });
    	
    };

}]);