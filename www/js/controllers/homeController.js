﻿'use strict';
app.controller('HomeCtrl',['$scope','$q','$location', 'clientService', 'sharedSurveyService','$rootScope','$ionicPopup','ionicToast','$timeout','storageService','surveyService',function ($scope,$q,$location, clientService,sharedSurveyService,$rootScope,$ionicPopup,ionicToast,$timeout,storageService,surveyService) {
   
    $scope.client = {};

var promises = [];

if($rootScope.isOnline){

           var surveyData= storageService.getsurveyData();
           console.log(surveyData);

           var arrayCount = surveyData.length;
           console.log(arrayCount);

if(arrayCount>0)
 {
for(var i=0; i<=arrayCount-1 ; i++)
{

var surveyObject=surveyData[i];
console.log(surveyObject);
 var promise=surveyService.saveSurvey(surveyObject);
promises.push(promise);

};
    $q.all(promises).then(

        function(results) {
           storageService.removesurveyData();
       
        });
};

    clientService.getClient().then(function (results) {
         storageService.removeclientData();
        $scope.client = results.data;

         storageService.addclientData($scope.client);
         $rootScope.isAdultClient=results.data.surveyAdultType;
         $rootScope.isGroupSrs=results.data.isGroupUser;
        //  alert($rootScope.isAdultClient);
        var prefix = results.data.surveyAdultType == true ? "" : "child";
        var surveytype = results.data.nextSurveyType == 2 ? 'SRS' : 'ORS';

       if (surveytype == 'SRS')
        {
         $scope.sessionNum = $scope.client.lastSession;
        };
        
        if (surveytype == 'ORS') 
        {
          $scope.sessionNum = $scope.client.lastSession + 1;
        };

        if (surveytype == 'SRS' && results.data.isGroupUser == true) {
            prefix = "group";
        }
        $scope.displaySurveytoTake = prefix != "" ? prefix + " " + surveytype : surveytype;
        $scope.surveytoTake = surveytype;

    }, function (error) {
       
    });

}

else{

var clientInfo = storageService.getclientData();
 $scope.client = clientInfo[0];

          $rootScope.isAdultClient= $scope.client.surveyAdultType;
         $rootScope.isGroupSrs= $scope.client.isGroupUser;

        var prefix = $scope.client.surveyAdultType == true ? "" : "child";
        var surveytype = $scope.client.nextSurveyType == 2 ? 'SRS' : 'ORS';

        if (surveytype == 'SRS')
        {
          $scope.sessionNum = $scope.client.lastSession;
        };
        
        if (surveytype == 'ORS') 
        {
          $scope.sessionNum = $scope.client.lastSession + 1;
        };

        if (surveytype == 'SRS' && $scope.client.isGroupUser == true) {
            prefix = "group";
        }

 $scope.displaySurveytoTake = prefix != "" ? prefix + " " + surveytype : surveytype;
        $scope.surveytoTake = surveytype;
       
};

}]);

