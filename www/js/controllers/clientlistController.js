'use strict';

 app.controller('ClientlistCtrl', ['$state','$q','$scope','$rootScope','ClientlistService','$ionicPopup','storageService','providersurveyService',function ($state,$q,$scope,$rootScope,ClientlistService,$ionicPopup,storageService,providersurveyService) {

// alert("clinet list controller");
$rootScope.isFromfeedbacklist=false;
$rootScope.isFromCouplelist=false;

    //    if (device.platform == 'iOS' && device.version >= '7.0') {
    //     //document.body.style.marginTop = "-20px";
    //     //document.getElementById("header").style.marginTop = "-20px";
    //     $('#header').css('margin-top', -20);
    //    // $('#header').css('height', 120);
    
    //     }
        //  alert(device.platform);
        //  alert(device.version);

// var screenWidth = window.screen.width;
// var screenHeight = window.screen.height;

// alert(screenWidth);
// alert(screenHeight);

// //     if(screenWidth < 360)
// //     {
// // $('#header').css('height', 120);
// // $('#content').css('top', 140);
// //     }

$scope.gotocouplelist = function(coupleId,clientName)
{
//     var coupleClient = [];
// alert(coupleId);
  $rootScope.coupleClientID=coupleId;
  $rootScope.coupleName=clientName;
//   $scope.individualClient = 
//     coupleClient.push[];

$state.go('tabprovider.couplelist');
};

$scope.gotofeedbacklist = function(parentID,clientName)
{
    // alert(parentID);
  $rootScope.parentClientID=parentID;
  $rootScope.parentName=clientName;
  $state.go('tabprovider.feedbacklist');
};
        $scope.gotoSrs = function (client) {

             $rootScope.clientId=client.userId;
             $rootScope.languageID=client.languageId;
             $rootScope.providergraphType=2;
            //  alert(JSON.stringify(client));
            //   $rootScope.isAdultClient=client.adulT_SURVEY_TYPE;
            $rootScope.isAdultClient=client.adulT_SURVEY_TYPE;

        //   $scope.isAdult=client.adulT_SURVEY_TYPE;
        //    $scope.isGroup=client.isGroupUser;
            $rootScope.isGroupSrs=client.isGroupUser;

            // var surveyType = client.surveyOrder == 2 ? 'SRS' : 'ORS';

    // if (surveyType == 'SRS' && $scope.isGroup == true) {

    //     $rootScope.isGroupSrs=true;
   
    //     }

        // else if($scope.isAdult == false)
        // {
        //      $state.go('tabprovider.childsrs');
        // }

        // else
        // {
        //     $rootScope.isGroupSrs=false;
        // }

         $state.go('tabprovider.srs');

    };

        $scope.gotoOrs = function (client) {
      
             $rootScope.clientId=client.userId;
             $rootScope.languageID=client.languageId;
              $rootScope.providergraphType=3;
            //   $rootScope.isAdultClient=client.adulT_SURVEY_TYPE;
             $rootScope.isAdultClient=client.adulT_SURVEY_TYPE;
                //    alert(JSON.stringify(client));
            $scope.isAdult=client.adulT_SURVEY_TYPE;
        //    $scope.isGroup=client.isGroupUser;


                //  if($scope.isAdult == false)
                //  {
                //      $state.go('tabprovider.childors');
                //  }

                //  else
                //  {
                // $state.go('tabprovider.ors');
                //  }

                  $state.go('tabprovider.ors');

    };

        $scope.gotoCsr = function (userId) {
       
              $rootScope.clientId=userId;
               $rootScope.providergraphType=1;
        $state.go('graph-provider');
    };


var promises = [];

if($rootScope.isOnline)
{

           var providersurveyData= storageService.getprovidersurveyData();
         
           var arrayCount = providersurveyData.length;
           console.log(arrayCount);

if(arrayCount>0)
 {
for(var i=0; i<=arrayCount-1 ; i++)
{

var surveyObject=providersurveyData[i];

var promise=providersurveyService.saveSurvey(surveyObject);
promises.push(promise);

};

    $q.all(promises).then(
        function(results) {
            storageService.removeprovidersurveyData();
        });
};

          ClientlistService.getClientlist()
                .then(function (results) {
                    $scope.clients = results;
                //  alert(JSON.stringify($scope.clients));
                    storageService.removeproviderData();
                     storageService.addproviderData($scope.clients);

                },function(err) {
                    var mes = "";
                     if (err.error_description == "" || err.error_description == undefined) mes = "Please try again later!";
                     else mes = err.error_description;

                     $ionicPopup.alert({
                         title: "No Response",
                         content: mes
                     })
                });

}

else
{

var providerData = storageService.getproviderData();
$scope.clients = providerData[0];

};   
     
     }]);
