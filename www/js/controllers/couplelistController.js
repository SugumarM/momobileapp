'use strict';

 app.controller('couplelistCtrl', ['$state','$q','$scope','$rootScope','CouplelistService','$ionicPopup','storageService','providersurveyService',function ($state,$q,$scope,$rootScope,CouplelistService,$ionicPopup,storageService,providersurveyService) {

    $scope.coupleID = $rootScope.coupleClientID;
    $scope.coupleName = $rootScope.coupleName;

         $scope.gotoSrs = function (client) {

             $rootScope.clientId=client.userId;
             $rootScope.languageID=client.languageId;
             $rootScope.providergraphType=2;
            //  alert(JSON.stringify(client));
  
            $rootScope.isAdultClient=client.adulT_SURVEY_TYPE;

            $rootScope.isGroupSrs=client.isGroupUser;

            $rootScope.isFromCouplelist=true;
            $rootScope.isFromfeedbacklist=false;

         $state.go('tabprovider.srs');

    };

        $scope.gotoOrs = function (client) {
      
             $rootScope.clientId=client.userId;
             $rootScope.languageID=client.languageId;
              $rootScope.providergraphType=3;
             $rootScope.isAdultClient=client.adulT_SURVEY_TYPE;
                  //  alert(JSON.stringify(client));
            $scope.isAdult=client.adulT_SURVEY_TYPE;

            $rootScope.isFromCouplelist=true;
            $rootScope.isFromfeedbacklist=false;

                  $state.go('tabprovider.ors');
    };

        $scope.gotoCsr = function (userId) {
       
              $rootScope.clientId=userId;
               $rootScope.providergraphType=1;
        $state.go('graph-provider');
    };

if($rootScope.isOnline)
{
          CouplelistService.getClientlist($scope.coupleID)
                .then(function (results) {
                    $scope.couples = results;
                    storageService.removecoupleList();
                     storageService.addcoupleList($scope.couples);
                    // alert(JSON.stringify($scope.couples));

                },function(err) {
                    var mes = "";
                     if (err.error_description == "" || err.error_description == undefined) mes = "Please try again later!";
                     else mes = err.error_description;

                     $ionicPopup.alert({
                         title: "No Response",
                         content: mes
                     })
                });
}

else
{

var coupleData = storageService.getcoupleList();
$scope.couples = coupleData[0];

};   
     
     }]);
