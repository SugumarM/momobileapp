app.controller('providerOrsCtrl', ['$scope', '$location', 'providersurveyService', 'sharedSurveyService', '$state', 'translationService','clientService','$ionicPopup','$timeout','storageService','$rootScope',function ($scope, $location, providersurveyService, sharedSurveyService, $state, translationService, clientService, $ionicPopup,$timeout,storageService,$rootScope) {


$scope.isAdult = $rootScope.isAdultClient;


$timeout(function () { $('input[type="range"]').rangeslider({ 
        polyfill: false, 
        onSlideEnd: function(position, value) {
           
            switch(this.$element.attr('id')) {
                case 'q1':
                    $scope.survey.Q1 = value;
                    break;
                case 'q2':
                    $scope.survey.Q2 = value;
                    break;
                case 'q3':
                    $scope.survey.Q3 = value;
                    break;
                case 'q4':
                    $scope.survey.Q4 = value;
                    break;
            }
            $scope.$apply();
           
        }
    });  }, 100);

if($rootScope.isOnline)
{
       $scope.langId = $rootScope.languageID;
        var a = translationService.getTranslation($scope);
}

else
{
           $scope.langId = $rootScope.languageID;
           var a = translationService.getTranslation($scope);
};

 
$scope.Gotoclientlist= function()
    {

if($rootScope.isFromCouplelist)
{
$state.go('tabprovider.couplelist');
}

else if($rootScope.isFromfeedbacklist)
{
 $state.go('tabprovider.feedbacklist');
}
else{
$state.go('tabprovider.clientlist');
}
        
    };


    $scope.survey = {
        // ClientId { get; set; }
        SurveyType: 1, // Ors=1 Srs=2
        ClientId : $rootScope.clientId,
        Q1: null,
        Q2: null,
        Q3: null,
        Q4: null,
        Total: null,
        // this is just a check probably won't be used in the code??
        Retake: false
    };

 $scope.feedback = sharedSurveyService.getOrsSurvey();
 $scope.feedbackTotal = $scope.feedback.Q1 + $scope.feedback.Q2 + $scope.feedback.Q3 + $scope.feedback.Q4;
    // we might have only one function that is submitSurvey
    $scope.Gotofeedback= function()
    {
       
// alert(JSON.stringify($scope.feedback));
        $state.go('tabprovider.orsfeedback');
    }

    $scope.submitOrs = function (survey) {

        var q=null;
                sharedSurveyService.addOrsQ1(q);
                sharedSurveyService.addOrsQ2(q);
                sharedSurveyService.addOrsQ3(q);
                sharedSurveyService.addOrsQ4(q);

        
        if($rootScope.isOnline)
        {
          
        var a = providersurveyService.saveSurvey($scope.survey)
        .then(function (results) {
        
           $state.go('graph-provider');
        
        });

        }

        else
        {
              storageService.addprovidersurveyData(survey);
               console.log(survey);
               $state.go('graph-provider');
        };

    };

    var warnSlider = function () {
        $ionicPopup.alert({
            title: "Use Slider",
            content: "Please use the slider!"
        })
            .then(function (result) {
                return;
             
            });
    }
    var s = sharedSurveyService.getOrsSurvey();
   
    $scope.survey.Q1 = s.Q1>=0 ? s.Q1 : null;
    $scope.survey.Q2 = s.Q2>=0 ? s.Q2 : null;
    $scope.survey.Q3 = s.Q3>=0 ? s.Q3 : null;
    $scope.survey.Q4 = s.Q4>=0 ? s.Q4 : null;


$scope.Gotoors=function()
{
$state.go('tabprovider.ors');

};

 $scope.Gotochildors=function()
{
    $state.go('tabprovider.childors');
};

$scope.Gotochildors1=function()
{
   $state.go('tabprovider.childors1');
};


     $scope.GoToQ1 = function () {
        $state.go('tabprovider.ors1');
    }

    $scope.GoToQ2 = function (q, page) {
        if ($scope.survey.Q1==null) {
            warnSlider();
        } 
 
        else {
            sharedSurveyService.addOrsQ1(q);
           
            $state.go(page);
        }
    }

    $scope.GoToQ3 = function (q, page) {
        if ($scope.survey.Q2 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addOrsQ2(q);
           
            $state.go(page);
        }
    }

    $scope.GoToQ4 = function (q, page) {
        if ($scope.survey.Q3 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addOrsQ3(q);
           
            $state.go(page);
        }
    }

    $scope.reviewOrs = function (q, page) {
        if ($scope.survey.Q4 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addOrsQ4(q);
            $state.go(page);
        }
       
    }


}]);

app.controller('providerSrsCtrl', ['$scope', '$location', 'providersurveyService', 'sharedSurveyService', '$state', 'translationService', 'clientService', '$ionicPopup' ,'$timeout','$rootScope','storageService',function ($scope, $location, providersurveyService, sharedSurveyService, $state, translationService, clientService,$ionicPopup,$timeout,$rootScope,storageService) {
  
  $scope.isGroup = $rootScope.isGroupSrs;
  $scope.isAdult = $rootScope.isAdultClient;

//   alert($scope.isGroup);
//   alert($scope.isAdult);

 $timeout(function () { $('input[type="range"]').rangeslider({ 
        polyfill: false, 
        onSlideEnd: function(position, value) {
        
            switch(this.$element.attr('id')) {
                case 'q1':
                    $scope.survey.Q1 = value;
                    break;
                case 'q2':
                    $scope.survey.Q2 = value;
                    break;
                case 'q3':
                    $scope.survey.Q3 = value;
                    break;
                case 'q4':
                    $scope.survey.Q4 = value;
                    break;
            }
            $scope.$apply();
            

        }
    });  }, 100);

 if($rootScope.isOnline)
{
            $scope.langId = $rootScope.languageID;
         
        var a = translationService.getTranslation($scope);
}

else
{
                $scope.langId = $rootScope.languageID;
        var a = translationService.getTranslation($scope);

};

    // $scope.navigateHome = function () {
    //     $state.go('tab.home');
    // };
 $scope.Gotoclientlist= function()
    {
if($rootScope.isFromCouplelist)
{
$state.go('tabprovider.couplelist');
}

else if($rootScope.isFromfeedbacklist)
{
 $state.go('tabprovider.feedbacklist');
}
else{
$state.go('tabprovider.clientlist');
}
    };

$scope.Gotogroupsrs=function()
{
     $state.go('tabprovider.groupsrs');
};

$scope.Gotogroupsrs1=function()
{
      $state.go('tabprovider.groupsrs1');
};

$scope.Gotochildsrs=function()
{
    $state.go('tabprovider.childsrs');
};

$scope.Gotochildsrs1=function()
{
    $state.go('tabprovider.childsrs1');
};

    $scope.survey = {
   // ClientId { get; set; }
    SurveyType:2, // Ors=1 Srs=2
    ClientId : $rootScope.clientId,
    Q1:null,
    Q2:null,
    Q3:null,
    Q4:null,
    Total:null,
    // this is just a check probably won't be used in the code??
    Retake:false
    };

    $scope.message = "";

     $scope.feedback = sharedSurveyService.getSrsSurvey();
 $scope.feedbackTotal = $scope.feedback.Q1 + $scope.feedback.Q2 + $scope.feedback.Q3 + $scope.feedback.Q4;
    // we might have only one function that is submitSurvey
    $scope.Gotofeedback= function()
    {
       
// alert(JSON.stringify($scope.feedback));
        $state.go('tabprovider.srsfeedback');
    }

    $scope.submitSrs = function (survey) {

        var q=null;
                sharedSurveyService.addSrsQ1(q);
                sharedSurveyService.addSrsQ2(q);
                sharedSurveyService.addSrsQ3(q);
                sharedSurveyService.addSrsQ4(q);

        if($rootScope.isOnline)
        {
   providersurveyService.saveSurvey($scope.survey)
        .then(function (results) {
          
           $state.go('graph-provider');
      
        });

        }

        else
        {
            storageService.addprovidersurveyData(survey);
               console.log(survey);
               $state.go('graph-provider');
        };
    
    };

    var s = sharedSurveyService.getSrsSurvey();

    $scope.survey.Q1 = s.Q1>=0 ? s.Q1 : null;
    $scope.survey.Q2 = s.Q2>=0 ? s.Q2 : null;
    $scope.survey.Q3 = s.Q3>=0 ? s.Q3 : null;
    $scope.survey.Q4 = s.Q4>=0 ? s.Q4 : null;

$scope.Gotosrs=function()
{
$state.go('tabprovider.srs');

}

    $scope.GoToQ1 = function () {
        $state.go('tabprovider.srs1');
    }

    var warnSlider = function () {
        $ionicPopup.alert({
            title: "Use Slider",
            content: "Please use the slider!"
        })
            .then(function (result) {
                return;
    
            });
    }

    $scope.GoToQ2 = function (q, page) {
        if ($scope.survey.Q1==null) {
            warnSlider();
        }else{
        sharedSurveyService.addSrsQ1(q);
        $state.go(page);
        }
    }

    $scope.GoToQ3 = function (q, page) {
        if ($scope.survey.Q2 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addSrsQ2(q);
            $state.go(page);
        }
    }

    $scope.GoToQ4 = function (q, page) {
        if ($scope.survey.Q3 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addSrsQ3(q);
            $state.go(page);
        }
    }

    $scope.reviewSrs = function (q, page) {
        if ($scope.survey.Q4 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addSrsQ4(q);
            $state.go(page);
        }
    }

}]);