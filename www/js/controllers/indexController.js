'use strict';
app.controller('IndexCtrl', ['$scope', '$state','$location','$rootScope','$cordovaNetwork', '$ionicPopup','ionicToast','$timeout',function ($scope, $state,$location,$rootScope,$cordovaNetwork,$ionicPopup,ionicToast,$timeout) {
    $scope.myname = "application";
    $scope.logOut = function () {
        authService.logOut();
        $location.path('/home');
    }

     document.addEventListener("deviceready", function () {

        $scope.network = $cordovaNetwork.getNetwork();
        $rootScope.isOnline = $cordovaNetwork.isOnline();
      
        $scope.$apply();

        // listen for Online event
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $rootScope.isOnline = true;

            $scope.network = $cordovaNetwork.getNetwork();
            
            $scope.$apply();
              $timeout(function() {
        ionicToast.hide();
    }, 5000);
        })

        // listen for Offline event
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){

            ionicToast.show('App is working in Offline mode', 'top', true);
            $rootScope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            
            $scope.$apply();
              $timeout(function() {
        ionicToast.hide();
    }, 5000);
        })

  }, false);
       

}]);