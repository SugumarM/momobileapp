﻿app.controller('OrsCtrl', ['$scope', '$location', 'surveyService', 'sharedSurveyService', '$state', 'translationService','clientService','$ionicPopup','$timeout','storageService','$rootScope',function ($scope, $location, surveyService, sharedSurveyService, $state, translationService, clientService, $ionicPopup,$timeout,storageService,$rootScope) {
        //submit ors

//  $scope.$on('$viewContentLoaded', function(){
//            $('input[type="range"]').rangeslider({ 
//         polyfill: false, 
//         onSlide: function(position, value) {
            
//             switch(this.$element.attr('id')) {
//                 case 'q1':
//                     $scope.survey.Q1 = value;
//                     break;
//                 case 'q2':
//                     $scope.survey.Q2 = value;
//                     break;
//                 case 'q3':
//                     $scope.survey.Q3 = value;
//                     break;
//                 case 'q4':
//                     $scope.survey.Q4 = value;
//                     break;
//             }
//             $scope.$apply();
//             console.log($scope.survey);

//         }
//         })
//         });

$scope.isAdult = $rootScope.isAdultClient;
// alert($scope.isAdult);



$timeout(function () { $('input[type="range"]').rangeslider({ 
        polyfill: false, 
        onSlideEnd: function(position, value) {
           
            switch(this.$element.attr('id')) {
                case 'q1':
                    $scope.survey.Q1 = value;
                    break;
                case 'q2':
                    $scope.survey.Q2 = value;
                    break;
                case 'q3':
                    $scope.survey.Q3 = value;
                    break;
                case 'q4':
                    $scope.survey.Q4 = value;
                    break;
            }
            $scope.$apply();
            // console.log($scope.survey);

        }
    });  }, 100);



if($rootScope.isOnline)
{
    clientService.getClient().then(function (results) {

        $scope.langId = results.data.languageId;
        var a = translationService.getTranslation($scope);

    }, function (error) {
        //alert(error.data.message);
    });

}

else
{
var clientInfo = storageService.getclientData();
 var clientData = clientInfo[0];

 $scope.langId = clientData.languageId;
        var a = translationService.getTranslation($scope);

};

    $scope.navigateHome = function () {
        $state.go('tab.home');
    };

$scope.Gotohome= function()
    {
        $state.go('tab.home');
    };



    $scope.survey = {
        // ClientId { get; set; }
        SurveyType: 1, // Ors=1 Srs=2
        
        Q1: null,
        Q2: null,
        Q3: null,
        Q4: null,
        Total: null,
        // this is just a check probably won't be used in the code??
        Retake: false
    };
    // console.log($scope.survey);
    // we might have only one function that is submitSurvey
     $scope.feedback = sharedSurveyService.getOrsSurvey();
 $scope.feedbackTotal = $scope.feedback.Q1 + $scope.feedback.Q2 + $scope.feedback.Q3 + $scope.feedback.Q4;
    // we might have only one function that is submitSurvey
$scope.Gotofeedback=function()
{
 $state.go('tab.orsfeedback');
}
   
    $scope.submitOrs = function (survey) {
        //$state.go('tab.graph');
        //console.log(survey);

       

   var q=null;
                sharedSurveyService.addOrsQ1(q);
                sharedSurveyService.addOrsQ2(q);
                sharedSurveyService.addOrsQ3(q);
                sharedSurveyService.addOrsQ4(q);
// alert(JSON.stringify($scope.survey));
        //    var surveyData= storageService.getsurveyData();
        //    console.log(surveyData);
        
        if($rootScope.isOnline)
        {
        var a = surveyService.saveSurvey($scope.survey)
        .then(function (results) {

// alert(JSON.stringify(results));
           $location.path("/graph/3");
            //$state.go('graph/2');
        });

        }

        else
        {
                    storageService.addsurveyData(survey);
               console.log(survey);
 $location.path('graph/3');
        };

        // var a = surveyService.saveSurvey($scope.survey)
        // .then(function (results) {

        //     $location.path("/graph/3");
        //     //$state.go('graph/3');

        // });
    };

    var warnSlider = function () {
        $ionicPopup.alert({
            title: "Use Slider",
            content: "Please use the slider!"
        })
            .then(function (result) {
                return;
                // if (!result) {
                //     //ionic.Platform.exitApp();
                // }
            });
    }
    var s = sharedSurveyService.getOrsSurvey();
   
    // $scope.survey.Q1 = s.Q1||null;
    // $scope.survey.Q2 = s.Q2 || null;
    // $scope.survey.Q3 = s.Q3 || null;
    // $scope.survey.Q4 = s.Q4 || null;

       $scope.survey.Q1 = s.Q1>=0 ? s.Q1 : null;
    $scope.survey.Q2 = s.Q2>=0 ? s.Q2 : null;
    $scope.survey.Q3 = s.Q3>=0 ? s.Q3 : null;
    $scope.survey.Q4 = s.Q4>=0 ? s.Q4 : null;


    // $scope.GoToQ1 = function (page) {
    //     $state.go(page);
    // }
$scope.Gotoors=function()
{
$state.go('tab.ors');

}

     $scope.GoToQ1 = function () {
        $state.go('tab.ors1');
    }

    $scope.Gotochildors1 = function()
    {
         $state.go('tab.childors1');
    }

    $scope.Gotochildors=function()
    {
        $state.go('tab.childors');
    }

    $scope.GoToQ2 = function (q, page) {
        if ($scope.survey.Q1==null) {
            warnSlider();
        } else {
            sharedSurveyService.addOrsQ1(q);
            //$location.path(page);
            $state.go(page);
        }
    }

    $scope.GoToQ3 = function (q, page) {
        if ($scope.survey.Q2 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addOrsQ2(q);
            //$location.path(page);
            $state.go(page);
        }
    }

    $scope.GoToQ4 = function (q, page) {
        if ($scope.survey.Q3 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addOrsQ3(q);
            //$location.path(page);
            $state.go(page);
        }
    }

    $scope.reviewOrs = function (q, page) {
        if ($scope.survey.Q4 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addOrsQ4(q);
            $state.go(page);
        }
        //$location.path(page);
    }


}]);

app.controller('SrsCtrl', ['$scope', '$location', 'surveyService', 'sharedSurveyService', '$state', 'translationService', 'clientService', '$ionicPopup' ,'$timeout','$rootScope','storageService',function ($scope, $location, surveyService, sharedSurveyService, $state, translationService, clientService,$ionicPopup,$timeout,$rootScope,storageService) {
        //submit ors

  $scope.isGroup = $rootScope.isGroupSrs;
  $scope.isAdult = $rootScope.isAdultClient;

        // $scope.$on('$viewContentLoaded', function(){
        //    $('input[type="range"]').rangeslider({ 
        // polyfill: false, 
        // onSlide: function(position, value) {
            
        //     switch(this.$element.attr('id')) {
        //         case 'q1':
        //             $scope.survey.Q1 = value;
        //             break;
        //         case 'q2':
        //             $scope.survey.Q2 = value;
        //             break;
        //         case 'q3':
        //             $scope.survey.Q3 = value;
        //             break;
        //         case 'q4':
        //             $scope.survey.Q4 = value;
        //             break;
        //     }
        //     $scope.$apply();
        //     console.log($scope.survey);

        // }
        // })
        // });

 $timeout(function () { $('input[type="range"]').rangeslider({ 
        polyfill: false, 
        onSlideEnd: function(position, value) {
        
            switch(this.$element.attr('id')) {
                case 'q1':
                    $scope.survey.Q1 = value;
                    break;
                case 'q2':
                    $scope.survey.Q2 = value;
                    break;
                case 'q3':
                    $scope.survey.Q3 = value;
                    break;
                case 'q4':
                    $scope.survey.Q4 = value;
                    break;
            }
            $scope.$apply();
            // console.log($scope.survey);

        }
    });  }, 100);

 if($rootScope.isOnline)
{
    clientService.getClient().then(function (results) {

        $scope.langId = results.data.languageId;
        var a = translationService.getTranslation($scope);

    }, function (error) {
        //alert(error.data.message);
    });

}

else
{
var clientInfo = storageService.getclientData();
 var clientData = clientInfo[0];

 $scope.langId = clientData.languageId;
        var a = translationService.getTranslation($scope);

};

    $scope.navigateHome = function () {
        $state.go('tab.home');
    };
    $scope.Gotohome= function()
    {
        $state.go('tab.home');
    };
    $scope.survey = {
   // ClientId { get; set; }
    SurveyType:2, // Ors=1 Srs=2
    Q1:null,
    Q2:null,
    Q3:null,
    Q4:null,
    Total:null,
    // this is just a check probably won't be used in the code??
    Retake:false
    };

    $scope.message = "";

     $scope.feedback = sharedSurveyService.getSrsSurvey();
 $scope.feedbackTotal = $scope.feedback.Q1 + $scope.feedback.Q2 + $scope.feedback.Q3 + $scope.feedback.Q4;
    // we might have only one function that is submitSurvey
    $scope.Gotofeedback= function()
    {
       
// alert(JSON.stringify($scope.feedback));
        $state.go('tab.srsfeedback');
    }


    $scope.submitSrs = function (survey) {

        //    var surveyData= storageService.getsurveyData();
        //    console.log(surveyData);
       var q=null;
                sharedSurveyService.addSrsQ1(q);
                sharedSurveyService.addSrsQ2(q);
                sharedSurveyService.addSrsQ3(q);
                sharedSurveyService.addSrsQ4(q);
        //$state.go('tab.graph');
        //console.log(survey);
        if($rootScope.isOnline)
        {
   surveyService.saveSurvey($scope.survey)
        .then(function (results) {
            // alert(JSON.stringify(results));
            $location.path('graph/2');
            //$state.go('graph/2');
        });

        }

        else
        {
                  storageService.addsurveyData(survey);
               console.log(survey);
 $location.path('graph/2');
        };
     
        // after survey is saved, go to graph page
        //$state.go('graph');
    };

    var s = sharedSurveyService.getSrsSurvey();

    
   $scope.survey.Q1 = s.Q1>=0 ? s.Q1 : null;
    $scope.survey.Q2 = s.Q2>=0 ? s.Q2 : null;
    $scope.survey.Q3 = s.Q3>=0 ? s.Q3 : null;
    $scope.survey.Q4 = s.Q4>=0 ? s.Q4 : null;

    // $scope.survey.Q1 = s.Q1 || null;
    // $scope.survey.Q2 = s.Q2 || null;
    // $scope.survey.Q3 = s.Q3 || null;
    // $scope.survey.Q4 = s.Q4 || null;

$scope.Gotosrs=function()
{
$state.go('tab.srs');

}
    $scope.GoToQ1 = function () {
        $state.go('tab.srs1');
    }

        $scope.Gotochildsrs1 = function()
    {
         $state.go('tab.childsrs1');
    }

    $scope.Gotochildsrs=function()
    {
        $state.go('tab.childsrs');
    }

    $scope.Gotogroupsrs1 = function()
    {
         $state.go('tab.groupsrs1');
    }

    $scope.Gotogroupsrs = function()
    {
         $state.go('tab.groupsrs');
    } 

    var warnSlider = function () {
        $ionicPopup.alert({
            title: "Use Slider",
            content: "Please use the slider!"
        })
            .then(function (result) {
                return;
                // if (!result) {
                //     //ionic.Platform.exitApp();
                // }
            });
    }

    $scope.GoToQ2 = function (q, page) {
        if ($scope.survey.Q1==null) {
            warnSlider();
        }else{
        sharedSurveyService.addSrsQ1(q);
        $state.go(page);
        }
    }

    $scope.GoToQ3 = function (q, page) {
        if ($scope.survey.Q2 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addSrsQ2(q);
            $state.go(page);
        }
    }

    $scope.GoToQ4 = function (q, page) {
        if ($scope.survey.Q3 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addSrsQ3(q);
            $state.go(page);
        }
    }

    $scope.reviewSrs = function (q, page) {
        if ($scope.survey.Q4 == null) {
            warnSlider();
        } else {
            sharedSurveyService.addSrsQ4(q);
            $state.go(page);
        }
    }

}])

;