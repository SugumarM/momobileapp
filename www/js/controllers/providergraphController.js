
app.controller('providerGraphCtrl', ['$scope','$location', 'providergraphService', '$state', 'clientService', '$rootScope','storageService',function ($scope,$location, providergraphService, $state, clientService, $rootScope,storageService) {

    // $scope.showFeedback = $rootScope.showFeedback;
    // $scope.graphTitle = "";

// alert("provider graph loaded");
// alert($rootScope.isOnline);

if($rootScope.providergraphType==2)
{
 $scope.graphType = {
        ClientId: $rootScope.clientId,

        graphType: 2
    };
};

if($rootScope.providergraphType==3)
{
 $scope.graphType = {
        ClientId: $rootScope.clientId,

        graphType: 3
    };
};

if($rootScope.providergraphType==1)
{

 $scope.graphType = {
        ClientId: $rootScope.clientId,

        graphType: 1
    };
};



// alert(JSON.stringify($scope.graphType));

//   "Both" : '1',
//         "Srs" : '2',
//         "Ors" : '3',
//         "Couple" : '4',
//         "FeedbackSources" : '5',
//         "Group" : '6',
//         "Ors2015" : '7'
//  headers: {
//                           "Accept": 'application/json',
//                           "Content-Type": 'application/json',
//                           "Authorization": 'Bearer providerauthData.access_token'
//                                 }

// alert(JSON.stringify($scope.graphType));

    $scope.navigatetoClientlist = function () {
        $state.go('tabprovider.clientlist');
    };

    // $scope.navigateFeedback = function () {
        
    //     if ($scope.graphType == 3) {
    //         $state.go('ors-feedback');
    //     }
    //     else {
    //         $state.go('srs-feedback');
    //     }
    // };

    $scope.graph = {};
    $scope.client = {};

    // we shouldn't need to call this here.... Let's find a different solution for this
    //clientService.getClient().then(function (results) {
        
    //    $scope.client = results.data;
    //    console.log(results.data);
    //    //$scope.showFeedback = $scope.client.showFeedback;

    //}, function (error) {
    //    //alert(error.data.message);
    //});
if($rootScope.isOnline)
{

    providergraphService.getGraph($scope.graphType).then(function (results) {

        $scope.graph = results.data;

    //    alert(JSON.stringify($scope.graph));

       if($rootScope.providergraphType==2)
       {
            // alert($rootScope.providergraphType);
   var temp={};
    temp.clientId=$rootScope.clientId;
temp.graphData=$scope.graph;

// alert(JSON.stringify(temp));

            storageService.removeprovidersrsgraphData();
       storageService.addprovidersrsgraphData(temp);
//                console.log($scope.graph);
// var graphInfo = storageService.getprovidersrsgraphData();
//   alert(graphInfo);
// $scope.graph = graphInfo[0];
// alert($scope.graph);

       };

      if($rootScope.providergraphType==3)
      {
        //    alert($rootScope.providergraphType);
           var temp={};
    temp.clientId=$rootScope.clientId;
temp.graphData=$scope.graph;

// alert(JSON.stringify(temp));
           storageService.removeproviderorsgraphData();
      storageService.addproviderorsgraphData(temp);
            //    console.log($scope.graph);
              
            // var graphInfo = storageService.getproviderorsgraphData();
            // alert(graphInfo);
// $scope.graph = graphInfo[0];
// alert($scope.graph);
      };

     if($rootScope.providergraphType==1)
      {
        //   alert($rootScope.providergraphType);
           var temp={};
    temp.clientId=$rootScope.clientId;
temp.graphData=$scope.graph;

// alert(JSON.stringify(temp));

          storageService.removeprovidercsrgraphData();
      storageService.addprovidercsrgraphData(temp);
    //    $scope.graph = graphInfo.;


        //        console.log($scope.graph);
        //    var graphInfo = storageService.getprovidercsrgraphData();
        //      alert(graphInfo);
// $scope.graph = graphInfo[0];
// alert($scope.graph);
      };
       

    }, function (error) {
        //alert(error.data.message);
    });
}

else{
    // alert("offline loaded");
      if($rootScope.providergraphType==2)
       {
        //    alert($rootScope.providergraphType);
         var graphInfo = storageService.getprovidersrsgraphData();
         var arrayCount= graphInfo.length;
        //  alert(graphInfo);
        if(arrayCount>0)
        {
        var graphObject=graphInfo[0];

       var claintID=graphObject.clientId;
       if(claintID==$rootScope.clientId)
       {
        $scope.graph = graphObject.graphData;
        $('#offlineText').html("<div style='position:fixed; width:100%; font-size: 12px; text-align:center'>Note: Since this device is currently offline - the graph will only show the most recent SRS scores that were uploaded to the MyOutcomes server. Once the device is connect to the internet - the graph will reflect the most recent scores entered.</div>");

       }
     else
       {
 $('#container').html("<div style='position:fixed; width:100%; font-size: 22px; text-align:center; top:45%'>No Graph Loaded</div>");
       };
        }
        else
       {
 $('#container').html("<div style='position:fixed; width:100%; font-size: 22px; text-align:center; top:45%'>No Graph Loaded</div>");
       }
  
// $scope.graph = graphInfo[0];
// alert(JSON.stringify($scope.graph));
       };

   if($rootScope.providergraphType==3)
      {
        //   alert($rootScope.providergraphType);
       var graphInfo = storageService.getproviderorsgraphData();
        // alert(graphInfo);
 var arrayCount= graphInfo.length;
        if(arrayCount>0)
        {
        var graphObject=graphInfo[0];

       var claintID=graphObject.clientId;
       if(claintID==$rootScope.clientId)
       {
        $scope.graph = graphObject.graphData;
$('#offlineText').html("<div style='position:fixed; width:100%; font-size: 12px; text-align:center'>Note: Since this device is currently offline - the graph will only show the most recent ORS scores that were uploaded to the MyOutcomes server. Once the device is connect to the internet - the graph will reflect the most recent scores entered.</div>");
       }

    else
       {
 $('#container').html("<div style='position:fixed; width:100%; font-size: 22px; text-align:center; top:45%'>No Graph Loaded</div>");
       };
        }
        else
       {
 $('#container').html("<div style='position:fixed; width:100%; font-size: 22px; text-align:center; top:45%'>No Graph Loaded</div>");
       };

// $scope.graph = graphInfo[0];
// alert(JSON.stringify($scope.graph));
      };

   if($rootScope.providergraphType==1)
      {
        //   alert($rootScope.providergraphType);
            var graphInfo = storageService.getprovidercsrgraphData();
 var arrayCount= graphInfo.length;
                //    var graphInfo = storageService.getprovidercrsgraphData();
           if(arrayCount>0)
        {
        var graphObject=graphInfo[0];

       var claintID=graphObject.clientId;
       if(claintID==$rootScope.clientId)
       {
        $scope.graph = graphObject.graphData;
$('#offlineText').html("<div style='position:fixed; width:100%; font-size: 12px; text-align:center'>Note: Since this device is currently offline - the graph will only show the most recent CSR scores that were uploaded to the MyOutcomes server. Once the device is connect to the internet - the graph will reflect the most recent scores entered.</div>");
       }

       else
       {
 $('#container').html("<div style='position:fixed; width:100%; font-size: 22px; text-align:center; top:45%'>No Graph Loaded</div>");
       };
        }
        else
       {
 $('#container').html("<div style='position:fixed; width:100%; font-size: 22px; text-align:center; top:45%'>No Graph Loaded</div>");
       }
            //  alert(graphInfo);
// $scope.graph = graphInfo[0];
// alert(JSON.stringify($scope.graph));
      };


};

    
}]);