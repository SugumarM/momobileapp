﻿'use strict';
app.factory('authInterceptorService', ['$q', '$location', 'localStorageService' 
    , function ($q, $location, localStorageService) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};
        
        var authData = localStorageService.get('authorizationData');
        //console.log(authData);
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }
        return config;
    }

    var _responseError = function (rejection) {
        
        if (rejection.status === 401) {
            $location.path('/login');
        }

        if (rejection.status === 0)
        {
           // $location.path('/connection');
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);