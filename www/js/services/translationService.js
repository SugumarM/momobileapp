﻿app.service('translationService', function($http) {  
    this.getTranslation = function($scope) {
        var languageFilePath = 'js/languages/translation_' + this.getLanguageCode($scope.langId) + '.json';
        //console.log(languageFilePath + "|" + $scope.langId);
        $http({ method: 'GET', url: languageFilePath }).success(function (data) {
            $scope.translation = data;
     
            return data;
        });
    };

    this.getLanguageCode = function (languageId) {
        switch (languageId) {

            case 2 : return 'es';
            case 3 : return 'dk';
            case 4 : return 'fr';
            case 7 : return 'de';
            case 8 : return 'sv';
            case 9 : return 'no';
            case 12: return 'nl';
       
            default: return 'en';
        }
    };
});