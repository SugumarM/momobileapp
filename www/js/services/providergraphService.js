'use strict';
app.factory('providergraphService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    //var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var graphServiceFactory = {};
// alert(ngAuthSettings.getBaseUrl() + 'api/graph/providerget');

    var _getGraph = function (graphType) {

        return $http.post(ngAuthSettings.getBaseUrl() + 'api/graph/providerget',graphType).then(function (results) {
            // alert(JSON.stringify(results));
            return results;
        });
    };


    graphServiceFactory.getGraph = _getGraph;

    return graphServiceFactory;

}]);


//$http.get(ngAuthSettings.getBaseUrl() + 'api/graph?graphType=' + graphType).then(function (results)