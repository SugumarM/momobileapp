﻿'use strict';
app.factory('authService', ['$http', '$rootScope','$q', 'localStorageService', 'ngAuthSettings','storageService','$ionicPopup'
    , function ($http,$rootScope, $q, localStorageService, ngAuthSettings,storageService,$ionicPopup) {


    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: "",
        useRefreshTokens: false
    };

    var _externalAuthData = {
        provider: "",
        userName: "",
        externalAccessToken: ""
    };

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(ngAuthSettings.getBaseUrl() + 'api/account/register', registration).then(function (response) {
            return response;
        });

    };

    var _login = function (loginData) {

             
        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        if (loginData.useRefreshTokens) {
            data = data + "&client_id=" + ngAuthSettings.clientId;
        }

        var deferred = $q.defer();
        
        $http.post(ngAuthSettings.getBaseUrl() + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded'} }).success(function (response) {

            if (loginData.useRefreshTokens) {
                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: response.refresh_token, useRefreshTokens: true });
            }
            else {
                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: "", useRefreshTokens: false });
            }
            localStorageService.set('loginData', { loginName: loginData.userName , password:loginData.password });
            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;
            _authentication.useRefreshTokens = loginData.useRefreshTokens;

$rootScope.providerdata=response;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {

        storageService.removeuserData();
        storageService.removeserverData();
        storageService.removeclientData();
        storageService.removesurveyData();
        storageService.removesrsgraphData();
        storageService.removeorsgraphData();
        storageService.removeprovidersrsgraphData();
        storageService.removeproviderorsgraphData();
        storageService.removeprovidercsrgraphData();
         storageService.removecoupleList();
        storageService.removefeedbackList();
        localStorageService.remove('authorizationData');
        localStorageService.remove('loginData');
        _authentication.isAuth = false;
        _authentication.userName = "";
        _authentication.useRefreshTokens = false;

    };

    var _fillAuthData = function () {
        
        var authData = localStorageService.get('authorizationData');
        //console.log(authData);
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
            _authentication.useRefreshTokens = authData.useRefreshTokens;
        }

    };

    var _refreshToken = function () {
        var deferred = $q.defer();

        var authData = localStorageService.get('authorizationData');

        if (authData) {

            if (authData.useRefreshTokens) {

                var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + ngAuthSettings.clientId;

                localStorageService.remove('authorizationData');

                $http.post(ngAuthSettings.getBaseUrl() + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                    localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });

                    deferred.resolve(response);

                }).error(function (err, status) {
                    _logOut();
                    deferred.reject(err);
                });
            }
        }

        return deferred.promise;
    };

    var _obtainAccessToken = function (externalData) {

        var deferred = $q.defer();

        $http.get(ngAuthSettings.getBaseUrl() + 'api/account/ObtainLocalAccessToken', { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } }).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });
            
            _authentication.isAuth = true;
            _authentication.userName = response.userName;
            _authentication.useRefreshTokens = false;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _registerExternal = function (registerExternalData) {

        var deferred = $q.defer();

        $http.post(ngAuthSettings.getBaseUrl() + 'api/account/registerexternal', registerExternalData).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });

            _authentication.isAuth = true;
            _authentication.userName = response.userName;
            _authentication.useRefreshTokens = false;



            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _getLoginData = function () {
        var loginData = localStorageService.get('loginData');
        return loginData;
    };



    // var _setServer = function (selected) {
    //     // localStorageService.set('serverData', { server: selected });
    //     // if (selected == "CA" || selected == "UK") {
    //     //     //config.baseUrl = 'http://localhost:53511/';
    //     //     ngAuthSettings.setBaseUrl('https://qc.myoutcomesapp.com/');
    //     //     //$scope.loginData = {
    //     //     //    userName: "c#400771",
    //     //     //    password: "111111"
    //     //     //};
    //     // } else if (selected == "US"|| selected == "INT") {
    //     //     ngAuthSettings.setBaseUrl('https://ws.myoutcomesapp.com/');
    //     //     //$scope.loginData = {
    //     //     //    userName: "c#400587",
    //     //     //    password: "w-du!.Of"
    //     //     //};

    //     // } 
    //     // else
    //     // {
    //     //     $ionicPopup.alert({
    //     //         title: "Please select your country!",
    //     //         content: ""
    //     //     }); 
    //     // }
    //     // else {
    //     //     //ngAuthSettings.setBaseUrl('https://ws.myoutcomesapp.com/');
    //     //     ngAuthSettings.setBaseUrl('http://rest.myoutcomesapp.com/');
    //     //     //$scope.loginData = {
    //     //     //    userName: "",
    //     //     //    password: ""
    //     //     //};
    //     // }
    // }

    // var _getServer = function () {
    //     var serverData = localStorageService.get('serverData');
    //     return serverData;
    // }

   // authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.refreshToken = _refreshToken;
    authServiceFactory.getLoginData = _getLoginData;
    // authServiceFactory.setServer = _setServer;
    // authServiceFactory.getServer = _getServer;
   
   // authServiceFactory.obtainAccessToken = _obtainAccessToken;
    //authServiceFactory.externalAuthData = _externalAuthData;
    //authServiceFactory.registerExternal = _registerExternal;

    return authServiceFactory;
}]);