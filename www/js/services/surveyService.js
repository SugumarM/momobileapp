﻿'use strict';
app.factory('surveyService', ['$http', 'ngAuthSettings','storageService',function ($http, ngAuthSettings,storageService) {

    //var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var surveyServiceFactory = {};

    var _saveSurvey = function (survey) {


// storageService.addsurveyData(survey);
//                console.log(survey);

//  var surveyInfo = storageService.getsurveyData();
//  console.log(surveyInfo);

        if (survey.retake == true) return _editSurvey(survey);
        else return _postSurvey(survey);
    }

    // could be just edit survey depending on retake true false?
    var _postSurvey = function (survey) {

        // maybe write a service helper and use
    //     alert(JSON.stringify(survey));
    //    alert(ngAuthSettings.getBaseUrl()+'api/survey/post');
        return $http.post(ngAuthSettings.getBaseUrl() + 'api/survey/post', survey).then(function (results) {
            //  alert(JSON.stringify(results));
            return results;
        });
    };

    var _editSurvey = function (survey) {

        return $http.put(ngAuthSettings.getBaseUrl() + 'api/survey/post', survey).then(function (results) {
            return results;
        });
    }

    surveyServiceFactory.saveSurvey = _saveSurvey;

    // storageService.addsurveyData(survey);
    //            console.log(survey);

    return surveyServiceFactory;

}]);