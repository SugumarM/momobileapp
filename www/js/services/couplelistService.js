
'use strict';
app.factory('CouplelistService',['$q','$http','$rootScope','ngAuthSettings', function($q,$http,$rootScope,ngAuthSettings) {

        return {

               getClientlist: function(coupleID) {
                //    alert("clinet list service");
                var deferred = $q.defer();
                var providerauthData=$rootScope.providerdata;
                //  var CoupleID=$rootScope.coupleClientID;
                // alert(JSON.stringify(providerauthData));
                var newData=[];
            //   alert(JSON.stringify(coupleID));
                 // $http.get('http://zaaroinfotechsolutions.com/zaarodemo/musicapp/api/v1/project/looplibrary')
                //  $http.get(ngAuthSettings.getBaseUrl() + 'api/moclient/get')
               $http.get(ngAuthSettings.getBaseUrl()+'api/provider/getchildclientlist?ClientId='+ coupleID ,{
                      headers: {
                          "Accept": 'application/json',
                          "Content-Type": 'application/json',
                          "Authorization": 'Bearer providerauthData.access_token'
                                }
                })
            .success(function(data, status, headers,config){
                        // alert(JSON.stringify(data));
                    newData=data;
                    deferred.resolve(newData); 
                }).error(function(data, status, headers,config){
                    deferred.reject();
                    // alert(JSON.stringify(status));
                    console.log('data error');
                }).then(function(result){
                    deferred.reject();
                });
                return deferred.promise;
            },

        }

    }]);