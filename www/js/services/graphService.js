﻿'use strict';
app.factory('graphService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    //var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var graphServiceFactory = {};


    var _getGraph = function (graphType) {

// alert(graphType);
        return $http.post(ngAuthSettings.getBaseUrl() + 'api/graph/get',graphType).then(function (results) {
             
            return results;
        });
    };

    graphServiceFactory.getGraph = _getGraph;

    return graphServiceFactory;

}]);



//  return $http.post(ngAuthSettings.getBaseUrl() + 'api/graph/providerget',graphType).then(function (results)