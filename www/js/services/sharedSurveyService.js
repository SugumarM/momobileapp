﻿// could be used inside surveyService? When we understand things better 
// we will make the right choice

app.service('sharedSurveyService', function () {
    var orsSurvey = {};
    var srsSurvey = {};
    var addOrsQ1 = function (q) {
        orsSurvey.Q1 = q;
    };
    var addOrsQ2 = function (q) {
        orsSurvey.Q2 = q;
    };
    var addOrsQ3 = function (q) {
        orsSurvey.Q3 = q;
    };
    var addOrsQ4 = function (q) {
        orsSurvey.Q4 = q;
    };

    var getOrsSurvey = function () {
        return orsSurvey;
    };

    var addSrsQ1 = function (q) {
        srsSurvey.Q1 = q;
    };
    var addSrsQ2 = function (q) {
        srsSurvey.Q2 = q;
    };
    var addSrsQ3 = function (q) {
        srsSurvey.Q3 = q;
    };
    var addSrsQ4 = function (q) {
        srsSurvey.Q4 = q;
    };
    var getSrsSurvey = function () {
        return srsSurvey;

    }

    return {
        addOrsQ1: addOrsQ1,
        addOrsQ2: addOrsQ2,
        addOrsQ3: addOrsQ3,
        addOrsQ4: addOrsQ4,
        getOrsSurvey: getOrsSurvey,
        addSrsQ1: addSrsQ1,
        addSrsQ2: addSrsQ2,
        addSrsQ3: addSrsQ3,
        addSrsQ4: addSrsQ4,
        getSrsSurvey: getSrsSurvey
    };

});