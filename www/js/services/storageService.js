app.factory ('storageService', function ($localStorage) {

  $localStorage = $localStorage.$default({
    userData: [],
    serverData:[],
    clientData: [],
    loginTime:[],
    surveyData: [],
    srsgraphData: [],
    orsgraphData:[],
    providerData: [],
    providersurveyData: [],
    providersrsgraphData:[],
    providerorsgraphData:[],
    providercsrgraphData:[],
    coupleList:[],
    feedbackList:[]
     
  });

   var _getuserData = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.userData;
  };

   var _getserverData = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.serverData;
  };

    var _getclientData = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.clientData;
  };

      var _getloginTime = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.loginTime;
  };

    var _getsurveyData = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.surveyData;
  };

    var _getsrsgraphData = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.srsgraphData;
  };

      var _getorsgraphData = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.orsgraphData;
  };

    var _getproviderData = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.providerData;
  };

    var _getprovidersurveyData = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.providersurveyData;
  };

    var _getprovidersrsgraphData = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.providersrsgraphData;
  };

    var _getproviderorsgraphData = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.providerorsgraphData;
  };

      var _getprovidercsrgraphData = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.providercsrgraphData;
  };

      var _getcoupleList = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.coupleList;
  };

        var _getfeedbackList = function () {
    //    var serverData = $localStorage.things('password');
    return $localStorage.feedbackList;
  };

    var _adduserData = function (loginData) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.userData.push(loginData);
  };

      var _addserverData = function (serverData) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.serverData.push(serverData);
  };

    var _addclientData = function (client) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.clientData.push(client);
  };

      var _addloginTime = function (loginTime) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.loginTime.push(loginTime);
  };

    var _addsurveyData = function (survey) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.surveyData.push(survey);
  };

     var _addsrsgraphData = function (graph) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.srsgraphData.push(graph);
  };

       var _addorsgraphData = function (graph) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.orsgraphData.push(graph);
  };
  
    var _addproviderData = function (providerData) {
    //    var serverData = $localStorage.things('password');
    $localStorage.providerData.push(providerData);
  };

    var _addprovidersurveyData = function (providersurveyData) {
    //    var serverData = $localStorage.things('password');
     $localStorage.providersurveyData.push(providersurveyData);
  };

     var _addprovidersrsgraphData = function (graph) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.providersrsgraphData.push(graph);
  };

    var _addproviderorsgraphData = function (graph) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.providerorsgraphData.push(graph);
  };

    var _addprovidercsrgraphData = function (graph) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.providercsrgraphData.push(graph);
  };

      var _addcoupleList = function (coupleList) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.coupleList.push(coupleList);
  };

      var _addfeedbackList = function (feedbackList) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.feedbackList.push(feedbackList);
  };
  // console.log($localStorage.surveyData);
  //  console.log($localStorage.userData);

  var _removeuserData = function (userData) {
    // $localStorage.things.splice(things);
    $localStorage.userData.splice($localStorage.userData);
    // $localStorage.remove(userData);
  };  

    var _removeserverData = function (serverData) {
    // $localStorage.things.splice(things);
    $localStorage.serverData.splice($localStorage.serverData);
    // $localStorage.remove(userData);
  };  

    var _removeclientData = function (clientData) {
    // $localStorage.things.splice(things);
    $localStorage.clientData.splice($localStorage.clientData);
    // $localStorage.remove(userData);
  };

      var _removeloginTime = function (loginTime) {
    // $localStorage.things.splice(things);
    $localStorage.loginTime.splice($localStorage.loginTime);
    // $localStorage.remove(userData);
  };

  var _removesurveyData = function (surveyData) {
    // $localStorage.things.splice(things);
    $localStorage.surveyData.splice($localStorage.surveyData);
    // $localStorage.remove(userData);
  };

   var _removesrsgraphData = function (graph) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.srsgraphData.splice($localStorage.srsgraphData);
  };

   var _removeorsgraphData = function (graph) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.orsgraphData.splice($localStorage.orsgraphData);
  };

     var _removeproviderData = function (providerData) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.providerData.splice($localStorage.providerData);
  };

     var _removeprovidersurveyData = function (providersurveyData) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.providersurveyData.splice($localStorage.providersurveyData);
  };

   var _removeprovidersrsgraphData = function (graph) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.providersrsgraphData.splice($localStorage.providersrsgraphData);
  };

     var _removeproviderorsgraphData = function (graph) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.providerorsgraphData.splice($localStorage.providerorsgraphData);
  };

   var _removeprovidercsrgraphData = function (graph) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.providercsrgraphData.splice($localStorage.providercsrgraphData);
  };

     var _removecoupleList = function (coupleList) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.coupleList.splice($localStorage.coupleList);
  };

       var _removefeedbackList = function (feedbackList) {
    //  var stringlogin = JSON.stringify(loginData)
    $localStorage.feedbackList.splice($localStorage.feedbackList);
  };

  return {

    getuserData: _getuserData,
    getserverData: _getserverData,
     getclientData: _getclientData,
      getloginTime: _getloginTime,
    getsurveyData: _getsurveyData,
    getsrsgraphData: _getsrsgraphData,
    getorsgraphData: _getorsgraphData,
     getproviderData: _getproviderData,
     getprovidersurveyData: _getprovidersurveyData,
      getprovidersrsgraphData: _getprovidersrsgraphData,
      getproviderorsgraphData: _getproviderorsgraphData,
      getprovidercsrgraphData: _getprovidercsrgraphData,
       getcoupleList: _getcoupleList,
        getfeedbackList: _getfeedbackList,
    adduserData: _adduserData,
     addserverData: _addserverData,
    addsurveyData: _addsurveyData,
    addclientData: _addclientData,
    addloginTime: _addloginTime,
    addsrsgraphData: _addsrsgraphData,
    addorsgraphData: _addorsgraphData,
     addproviderData: _addproviderData,
    addprovidersurveyData: _addprovidersurveyData,
    addprovidersrsgraphData: _addprovidersrsgraphData,
    addproviderorsgraphData: _addproviderorsgraphData,
     addprovidercsrgraphData: _addprovidercsrgraphData,
      addcoupleList: _addcoupleList,
      addfeedbackList: _addfeedbackList,
    removeuserData: _removeuserData,
    removeserverData: _removeserverData,
     removeclientData: _removeclientData,
     removeloginTime: _removeloginTime,
      removesurveyData: _removesurveyData,
       removesrsgraphData: _removesrsgraphData,
       removeorsgraphData: _removeorsgraphData,
       removeproviderData: _removeproviderData,
       removeprovidersurveyData: _removeprovidersurveyData,
       removeprovidersrsgraphData: _removeprovidersrsgraphData,
       removeproviderorsgraphData: _removeproviderorsgraphData,
       removeprovidercsrgraphData: _removeprovidercsrgraphData,
       removecoupleList: _removecoupleList,
       removefeedbackList: _removefeedbackList
  };
});