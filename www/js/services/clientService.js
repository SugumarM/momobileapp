﻿'use strict';
app.factory('clientService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    //var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var clientServiceFactory = {};

    var client = {};

    var _getClient = function () {

        return $http.get(ngAuthSettings.getBaseUrl() + 'api/moclient/get').then(function (results) {
            //console.log(results);
            //alert(JSON.stringify(results));
            client = results.data;
            return results;
        });
    };

    clientServiceFactory.getClient = _getClient;
    clientServiceFactory.client = client;
    return clientServiceFactory;

}]);