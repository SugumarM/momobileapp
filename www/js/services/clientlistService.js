'use strict';
app.factory('ClientlistService',['$q','$http','$rootScope','ngAuthSettings', function($q,$http,$rootScope,ngAuthSettings) {

        // We use promises to make this api asynchronous. This is clearly not necessary when using in-memory data
        // but it makes this service more flexible and plug-and-play. For example, you can now easily replace this
        // service with a JSON service that gets its data from a remote server without having to changes anything
        // in the modules invoking the data service since the api is already async.

        return {

               getClientlist: function() {
                //    alert("clinet list service");
                var deferred = $q.defer();
                var providerauthData=$rootScope.providerdata;
                //alert(JSON.stringify(providerData));
                var newData=[];
              
                 // $http.get('http://zaaroinfotechsolutions.com/zaarodemo/musicapp/api/v1/project/looplibrary')
                //  $http.get(ngAuthSettings.getBaseUrl() + 'api/moclient/get')
               $http.get(ngAuthSettings.getBaseUrl()+'api/provider/getclientlist', {
                      headers: {
                          "Accept": 'application/json',
                          "Content-Type": 'application/json',
                          "Authorization": 'Bearer providerauthData.access_token'
                                }
                })
            .success(function(data, status, headers,config){
                        // alert(JSON.stringify(data));
                    newData=data;
                    deferred.resolve(newData); 
                }).error(function(data, status, headers,config){
                    deferred.reject();
                    // alert(JSON.stringify(status));
                    console.log('data error');
                }).then(function(result){
                    deferred.reject();
                });
                return deferred.promise;
            },

            // findById: function(clientId) {
            //     var deferred = $q.defer();
            //     var client = clients[clientId - 1];
            //     deferred.resolve(clients);
            //     return deferred.promise;
            // },

            // findByName: function(searchKey) {
            //     var deferred = $q.defer();
            //     var results = clients.filter(function(element) {
            //         var fullName = element.firstName + " " + element.lastName;
            //         return fullName.toLowerCase().indexOf(searchKey.toLowerCase()) > -1;
            //     });
            //     deferred.resolve(results);
            //     return deferred.promise;
            // },

        }

    }]);