// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter',
    ['ionic'
        , 'starter.highcharts'
        , 'LocalStorageModule'
        , 'chieffancypants.loadingBar'
        , 'ngCordova'
        ,'ngStorage'
        ,'ngCordova.plugins'
        ,'ionic-toast'
 
    ]);

app.run(function($ionicPlatform,$cordovaNetwork, $rootScope) {

  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    
    if(window.StatusBar) {
      StatusBar.styleDefault();
      StatusBar.overlaysWebView(false);
      StatusBar.backgroundColorByHexString("#82b823");
    }

  });

  $ionicPlatform.registerBackButtonAction(function (event) {
           event.preventDefault();
      }, 100);

})

.config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

        .state('signin', {
            url: '/sign-in',
            templateUrl: 'templates/sign-in.html',
            controller: 'SignInCtrl'
        })

      // setup an abstract state for the tabs directive
      .state('tab', {
          url: "/tab",
          abstract: true,
          templateUrl: "templates/tabs.html",
          controller: 'TabCtrl'
      })
       .state('tabprovider', {
          url: "/tabprovider",
          abstract: true,
          templateUrl: "templates/provider-tabs.html",
          controller: 'providerTabCtrl'
      })

      .state('tabprovider.clientlist', {
                url: '/client-list',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/client-list.html',
                controller: 'ClientlistCtrl'
                }
            }
                
            })

    .state('tabprovider.orsfeedback', {
          url: '/provider-ors-feedback',
          views: {
              'tab-provider': {
                 templateUrl: 'templates/provider-ors-feedback.html',
                  controller: 'providerOrsCtrl' //we might change this to SurveyCtrl
              }
          }
      })

          .state('tabprovider.srsfeedback', {
          url: '/provider-srs-feedback',
          views: {
              'tab-provider': {
                 templateUrl: 'templates/provider-srs-feedback.html',
                  controller: 'providerSrsCtrl' //we might change this to SurveyCtrl
              }
          }
      })

                .state('tabprovider.couplelist', {
          url: '/couple-list',
          views: {
              'tab-provider': {
                 templateUrl: 'templates/couple-list.html',
                  controller: 'couplelistCtrl' //we might change this to SurveyCtrl
              }
          }
      })

                .state('tabprovider.feedbacklist', {
          url: '/feedbacksource-list',
          views: {
              'tab-provider': {
                 templateUrl: 'templates/feedbacksource-list.html',
                  controller: 'feedbacklistCtrl' //we might change this to SurveyCtrl
              }
          }
      })
///////////////////////////////////////////////////
                  .state('tabprovider.srs', {
                url: '/provider-srs',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-srs.html',
                controller: 'providerSrsCtrl'
                }
            }
                
            })

                  .state('tabprovider.srs1', {
                url: '/provider-srs1',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-srs1.html',
                controller: 'providerSrsCtrl'
                }
            }
                
            })

                    .state('tabprovider.srs2', {
                url: '/provider-srs2',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-srs2.html',
                controller: 'providerSrsCtrl'
                }
            }
                
            })

                    .state('tabprovider.srs3', {
                url: '/provider-srs3',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-srs3.html',
                controller: 'providerSrsCtrl'
                }
            }
                
            })

                    .state('tabprovider.srs4', {
                url: '/provider-srs4',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-srs4.html',
                controller: 'providerSrsCtrl'
                }
            }
                
            })

                 .state('tabprovider.srsreview', {
                url: '/provider-srs-review',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-srs-review.html',
                controller: 'providerSrsCtrl'
                }
            }
                
            })
/////////////////////////////////////////////////
                  .state('tabprovider.ors', {
                url: '/provider-ors',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-ors.html',
                controller: 'providerOrsCtrl'
                }
            }
                
            })

                  .state('tabprovider.ors1', {
                url: '/provider-ors1',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-ors1.html',
                controller: 'providerOrsCtrl'
                }
            }
                
            })

                 .state('tabprovider.ors2', {
                url: '/provider-ors2',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-ors2.html',
                controller: 'providerOrsCtrl'
                }
            }
                
            })

                 .state('tabprovider.ors3', {
                url: '/provider-ors3',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-ors3.html',
                controller: 'providerOrsCtrl'
                }
            }
                
            })

                 .state('tabprovider.ors4', {
                url: '/provider-ors4',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-ors4.html',
                controller: 'providerOrsCtrl'
                }
            }
                
            })

                 .state('tabprovider.orsreview', {
                url: '/provider-ors-review',
                views: {
                'tab-provider': {
                   templateUrl: 'templates/provider-ors-review.html',
                controller: 'providerOrsCtrl'
                }
            }
                
            })

////////////////////////////////

    //   .state('tabprovider.childsrs', {
    //             url: '/provider-child-srs',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-srs.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

    //               .state('tabprovider.childsrs1', {
    //             url: '/provider-child-srs1',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-srs1.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

    //                 .state('tabprovider.childsrs2', {
    //             url: '/provider-child-srs2',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-srs2.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

    //                 .state('tabprovider.childsrs3', {
    //             url: '/provider-child-srs3',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-srs3.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

    //                 .state('tabprovider.childsrs4', {
    //             url: '/provider-child-srs4',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-srs4.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

    //              .state('tabprovider.childsrsreview', {
    //             url: '/provider-child-srs-review',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-srs-review.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

///////////////////////////////

    //   .state('tabprovider.groupsrs', {
    //             url: '/provider-group-srs',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-group-srs.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

    //               .state('tabprovider.groupsrs1', {
    //             url: '/provider-group-srs1',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-group-srs1.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

    //                 .state('tabprovider.groupsrs2', {
    //             url: '/provider-group-srs2',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-group-srs2.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

    //                 .state('tabprovider.groupsrs3', {
    //             url: '/provider-group-srs3',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-group-srs3.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

    //                 .state('tabprovider.groupsrs4', {
    //             url: '/provider-group-srs4',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-group-srs4.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

    //              .state('tabprovider.groupsrsreview', {
    //             url: '/provider-group-srs-review',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-group-srs-review.html',
    //             controller: 'providerSrsCtrl'
    //             }
    //         }
                
    //         })

///////////////////////////////

    // .state('tabprovider.childors', {
    //             url: '/provider-child-ors',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-ors.html',
    //             controller: 'providerOrsCtrl'
    //             }
    //         }
                
    //         })

    //               .state('tabprovider.childors1', {
    //             url: '/provider-child-ors1',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-ors1.html',
    //             controller: 'providerOrsCtrl'
    //             }
    //         }
                
    //         })

    //              .state('tabprovider.childors2', {
    //             url: '/provider-child-ors2',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-ors2.html',
    //             controller: 'providerOrsCtrl'
    //             }
    //         }
                
    //         })

    //              .state('tabprovider.childors3', {
    //             url: '/provider-child-ors3',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-ors3.html',
    //             controller: 'providerOrsCtrl'
    //             }
    //         }
                
    //         })

    //              .state('tabprovider.childors4', {
    //             url: '/provider-child-ors4',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-ors4.html',
    //             controller: 'providerOrsCtrl'
    //             }
    //         }
                
    //         })

    //              .state('tabprovider.childorsreview', {
    //             url: '/provider-child-ors-review',
    //             views: {
    //             'tab-provider': {
    //                templateUrl: 'templates/provider-child-ors-review.html',
    //             controller: 'providerOrsCtrl'
    //             }
    //         }
                
    //         })

//////////////////////////////

        .state('index', { templateUrl: "index.html", controller: 'indexCtrl' })
      // Each tab has its own nav history stack:

        .state('tab.home', {
            url: '/home',
            views: {
                'tab-home': {
                    templateUrl: 'templates/home.html',
                    controller: 'HomeCtrl'
                }
            }
        })

      .state('graph', {
          url: '/graph/:gt',
          templateUrl: 'templates/graph.html',
          controller: 'GraphCtrl'
      })

  .state('graph-provider', {
          url: '/graph-provider',
          templateUrl: 'templates/graph-provider.html',
          controller: 'providerGraphCtrl'
      })

            .state('tab.orsfeedback', {
          url: '/ors-feedback',
          views: {
              'tab-home': {
                 templateUrl: 'templates/ors-feedback.html',
                  controller: 'OrsCtrl' //we might change this to SurveyCtrl
              }
          }
      })

            .state('tab.srsfeedback', {
          url: '/srs-feedback',
          views: {
              'tab-home': {
                 templateUrl: 'templates/srs-feedback.html',
                  controller: 'SrsCtrl' //we might change this to SurveyCtrl
              }
          }
      })

    //   .state('srs-feedback', {
    //       url: '/srs-feedback',

    //       templateUrl: 'templates/srs-feedback.html',
    //       controller: 'SrsFeedbackCtrl'

    //   })

      .state('tab.ors', {
          url: '/ORS',
          views: {
              'tab-home': {
                  templateUrl: 'templates/ors.html',
                  controller: 'OrsCtrl' //we might change this to SurveyCtrl
              }
          }
      })
      .state('tab.ors1', {
            url: '/ORS1',
            views: {
                'tab-home': {
                    templateUrl: 'templates/ors1.html',
                    controller: 'OrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.ors2', {
            url: '/ORS2',
            views: {
                'tab-home': {
                    templateUrl: 'templates/ors2.html',
                    controller: 'OrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.ors3', {
            url: '/ORS3',
            views: {
                'tab-home': {
                    templateUrl: 'templates/ors3.html',
                    controller: 'OrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.ors4', {
            url: '/ORS4',
            views: {
                'tab-home': {
                    templateUrl: 'templates/ors4.html',
                    controller: 'OrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.orsreview', {
            url: '/ORSreview',
            views: {
                'tab-home': {
                    templateUrl: 'templates/ors-review.html',
                    controller: 'OrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.childors', {
            url: '/childORS',
            views: {
                'tab-home': {
                    templateUrl: 'templates/child-ors.html',
                    controller: 'OrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
      .state('tab.childors1', {
          url: '/childORS1',
          views: {
              'tab-home': {
                  templateUrl: 'templates/child-ors1.html',
                  controller: 'OrsCtrl' //we might change this to SurveyCtrl
              }
          }
      })
        .state('tab.childors2', {
            url: '/childORS2',
            views: {
                'tab-home': {
                    templateUrl: 'templates/child-ors2.html',
                    controller: 'OrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.childors3', {
            url: '/childORS3',
            views: {
                'tab-home': {
                    templateUrl: 'templates/child-ors3.html',
                    controller: 'OrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.childors4', {
            url: '/childORS4',
            views: {
                'tab-home': {
                    templateUrl: 'templates/child-ors4.html',
                    controller: 'OrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.childorsreview', {
            url: '/childORSreview',
            views: {
                'tab-home': {
                    templateUrl: 'templates/child-ors-review.html',
                    controller: 'OrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.srs', {
            url: '/SRS',
            views: {
                'tab-home': {
                    templateUrl: 'templates/srs.html',
                    controller: 'SrsCtrl'
                }
            }
        })
      .state('tab.srs1', {
          url: '/SRS1',
          views: {
              'tab-home': {
                  templateUrl: 'templates/srs1.html',
                  controller: 'SrsCtrl' //we might change this to SurveyCtrl
              }
          }
      })
        .state('tab.srs2', {
            url: '/SRS2',
            views: {
                'tab-home': {
                    templateUrl: 'templates/srs2.html',
                    controller: 'SrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.srs3', {
            url: '/SRS3',
            views: {
                'tab-home': {
                    templateUrl: 'templates/srs3.html',
                    controller: 'SrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.srs4', {
            url: '/SRS4',
            views: {
                'tab-home': {
                    templateUrl: 'templates/srs4.html',
                    controller: 'SrsCtrl' //we might change this to SurveyCtrl
                }
            }
        })
        .state('tab.srsreview', {
            url: '/SRSreview',
            views: {
                'tab-home': {
                    templateUrl: 'templates/srs-review.html',
                    controller: 'SrsCtrl' //we might change this to SurveyCtrl
                }
            }
        });
        /////////////////////////////////////
    //     .state('tab.childsrs', {
    //         url: '/childSRS',
    //         views: {
    //             'tab-home': {
    //                 templateUrl: 'templates/child-srs.html',
    //                 controller: 'SrsCtrl'
    //             }
    //         }
    //     })
    //   .state('tab.childsrs1', {
    //       url: '/childSRS1',
    //       views: {
    //           'tab-home': {
    //               templateUrl: 'templates/child-srs1.html',
    //               controller: 'SrsCtrl' //we might change this to SurveyCtrl
    //           }
    //       }
    //   })
    //     .state('tab.childsrs2', {
    //         url: '/childSRS2',
    //         views: {
    //             'tab-home': {
    //                 templateUrl: 'templates/child-srs2.html',
    //                 controller: 'SrsCtrl' //we might change this to SurveyCtrl
    //             }
    //         }
    //     })
    //     .state('tab.childsrs3', {
    //         url: '/childSRS3',
    //         views: {
    //             'tab-home': {
    //                 templateUrl: 'templates/child-srs3.html',
    //                 controller: 'SrsCtrl' //we might change this to SurveyCtrl
    //             }
    //         }
    //     })
    //     .state('tab.childsrs4', {
    //         url: '/childSRS4',
    //         views: {
    //             'tab-home': {
    //                 templateUrl: 'templates/child-srs4.html',
    //                 controller: 'SrsCtrl' //we might change this to SurveyCtrl
    //             }
    //         }
    //     })
    //     .state('tab.childsrsreview', {
    //         url: '/childSRSreview',
    //         views: {
    //             'tab-home': {
    //                 templateUrl: 'templates/child-srs-review.html',
    //                 controller: 'SrsCtrl' //we might change this to SurveyCtrl
    //             }
    //         }
    //     })
////////////////////////////
                /////////////////////////////////////
        
    //     .state('tab.groupsrs', {
    //         url: '/groupSRS',
    //         views: {
    //             'tab-home': {
    //                 templateUrl: 'templates/group-srs.html',
    //                 controller: 'SrsCtrl'
    //             }
    //         }
    //     })
    //   .state('tab.groupsrs1', {
    //       url: '/groupSRS1',
    //       views: {
    //           'tab-home': {
    //               templateUrl: 'templates/group-srs1.html',
    //               controller: 'SrsCtrl' //we might change this to SurveyCtrl
    //           }
    //       }
    //   })
    //     .state('tab.groupsrs2', {
    //         url: '/groupSRS2',
    //         views: {
    //             'tab-home': {
    //                 templateUrl: 'templates/group-srs2.html',
    //                 controller: 'SrsCtrl' //we might change this to SurveyCtrl
    //             }
    //         }
    //     })
    //     .state('tab.groupsrs3', {
    //         url: '/groupSRS3',
    //         views: {
    //             'tab-home': {
    //                 templateUrl: 'templates/group-srs3.html',
    //                 controller: 'SrsCtrl' //we might change this to SurveyCtrl
    //             }
    //         }
    //     })
    //     .state('tab.groupsrs4', {
    //         url: '/groupSRS4',
    //         views: {
    //             'tab-home': {
    //                 templateUrl: 'templates/group-srs4.html',
    //                 controller: 'SrsCtrl' //we might change this to SurveyCtrl
    //             }
    //         }
    //     })
    //     .state('tab.groupsrsreview', {
    //         url: '/groupSRSreview',
    //         views: {
    //             'tab-home': {
    //                 templateUrl: 'templates/group-srs-review.html',
    //                 controller: 'SrsCtrl' //we might change this to SurveyCtrl
    //             }
    //         }
    //     })

///////////////////////////

        // .state('tab.srs.feedback', {
        //     url: '/srs-feedback',
        //     views: {
        //         'srs-feedback': {
        //             templateUrl: 'templates/srs-feedback.html',
        //             controller: 'SrsFeedbackCtrl'
        //         }
        //     }
        // });

    // if none of the above states are matched, use this as the fallback
  
    $urlRouterProvider.otherwise('/sign-in');
});


app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});
// should be dynamic with server selection on login page
var serviceBase = 'http://test.myoutcomesapp.com/';

app.service('ngAuthSettings', function () {
    var baseUrl = 'http://rest.myoutcomesapp.com/';

    var _setBaseUrl = function (uri) {
        baseUrl = uri;
    };
    var _getBaseUrl = function () {
        return baseUrl;
    }

    return {
        setBaseUrl: _setBaseUrl,
        getBaseUrl: _getBaseUrl
    }
});

var config = { baseUrl: serviceBase };


app.run(['authService', function (authService) {
    authService.fillAuthData();
}]);


