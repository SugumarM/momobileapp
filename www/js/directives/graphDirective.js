﻿
angular.module('starter.highcharts', [])
.directive('drawPieChart', function () {
    return function ($scope, element, attrs) {

        var container = $(element).attr("id");

        // watch the expression, and update the UI on change.
        $scope.$watch('graph', function () {
            // alert(JSON.stringify($scope.graph));
            if(Object.keys($scope.graph).length == 0)
            {
                // alert("NUll Data");
        //    $('#container').html("<div style='position:fixed; width:100%; font-size: 22px; text-align:center; top:45%'>No Graph Loaded</div>");
            }
            else
            {
             drawPlot($scope.graph);
            };
            
        }, true);

        var drawPlot = function (data) {
            var chart;
            //console.log(angular.isObject(data));
            if(data.title.text=="Session Rating Scale")
            {
             data.series[0].color ="#485A9C";
             data.series[1].color ="#4EA342";
             data.series[0].marker.symbol="diamond";
             data.series[1].marker.symbol="circle";
             data.series[1].marker.fillColor="#b3597c";
             data.series[0].marker.fillColor="#b3597c";
             data.series[1].marker.lineColor="#4EA342";
             data.series[0].marker.lineColor="#485A9C";
            //  data.series[1].marker.lineWidth=1;
             
           //    data.legend.backgroundColor="#605E16";
            data.plotOptions.area.lineColor="#F0C8C8";  
            data.xAxis[0].labels.style.color="#640000";
            data.xAxis[1].labels.style.color="#640000";
            data.yAxis.labels.style.color="#0C5A01";
            data.chart.plotShadow="true";
            data.chart.plotBackgroundColor="#D8F3E3";
            data.plotOptions.color="#FF000D";
            }
            
            else
            {
                data.series[4].color ="#485A9C";
                data.series[5].color ="#4EA342";
                data.series[4].marker.symbol="diamond";
                data.series[5].marker.symbol="circle";
                data.series[5].marker.fillColor="#b3597c";
                data.series[4].marker.fillColor="#b3597c";
                data.series[5].marker.lineColor="#4EA342";
             data.series[4].marker.lineColor="#485A9C";
             
             //data.plotOptions.area.lineColor="#F0C8C8";  
             data.xAxis[0].labels.style.color="#640000";
             data.xAxis[1].labels.style.color="#640000";
             data.yAxis.labels.style.color="#0C5A01";
            data.chart.plotShadow="true";
            data.chart.plotBackgroundColor="#D8F3E3";
            data.plotOptions.color="#FF000D";
               
            }
            
            // data.chart.plotBorderWidth=5;
            if (Object.keys(data).length > 0) {
                data.chart.width = null;
                data.chart.height = null;
            }

            chart =
                new Highcharts.Chart(data
            //    {
            //    "chart": {
            //        "renderTo": "container",
            //        //"width": %65,
            //        //"height": %45,
            //        "marginLeft": 25,
            //        "zoomType": "x"
            //    },
            //    "title": {
            //        "text": "Expected Treatment Response (ETR)"
            //    },
            //    "legend": {
            //        "layout": "horizontal",
            //        "align": "center",
            //        "verticalAlign": "bottom",
            //        "floating": false,
            //        "borderWidth": 1,
            //        "backgroundColor": "#FFFFFF",
            //        "reversed": true,
            //        "y": 0
            //    },
            //    "xAxis": [{
            //        "title": {
            //            "text": "Session"
            //        },
            //        "categories": ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18"],
            //        "tickmarkPlacement": "on",
            //        "labels": {
            //            "align": "center",
            //            "x": 0,
            //            "y": 18,
            //            "staggerLines": 0,
            //            "style": {
            //                "color": null,
            //                "fontSize": "11px"
            //            }
            //        },
            //        "opposite": false,
            //        "linkedTo": null
            //    }, {
            //        "title": {
            //            "text": ""
            //        },
            //        "categories": ["10/31/2012", "11/7/2012", "11/14/2012", "11/21/2012", "11/2/2012", "11/27/2012", "4/30/2013", "4/30/2013", "4/30/2013", "", "", "", "", "", "", "", "", ""],
            //        "tickmarkPlacement": "on",
            //        "labels": {
            //            "align": "center",
            //            "x": 0,
            //            "y": -25,
            //            "staggerLines": 2,
            //            "style": {
            //                "color": null,
            //                "fontSize": "9px"
            //            }
            //        },
            //        "opposite": true,
            //        "linkedTo": 0
            //    }],
            //    "yAxis": {
            //        "title": {
            //            "text": "Score"
            //        },
            //        "max": 45,
            //        "min": -5,
            //        "labels": {
            //            "align": "left",
            //            "x": -7,
            //            "y": -2,
            //            "staggerLines": 0,
            //            "style": {
            //                "color": null,
            //                "fontSize": "9px"
            //            }
            //        },
            //        "endOnTick": false,
            //        "startOnTick": false
            //    },
            //    "credits": {
            //        "enabled": false
            //    },
            //    "plotOptions": {
            //        "area": {
            //            "fillOpacity": 0.6,
            //            "lineWidth": 0
            //        }
            //    },
            //    "colors": ["#81c031", "#5281ff", "#336600", "#000006", "#3655a8", "#800000", "#f2822f", "#CC0000", "#ebf21d"],
            //    "series": [{
            //        "name": "Above ETR",
            //        "type": "area",
            //        "data": [40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40],
            //        "marker": {
            //            "enabled": false,
            //            "symbol": null
            //        }
            //    }, {
            //        "name": "Below ETR",
            //        "type": "area",
            //        "data": [15.50, 19.10, 21.18, 22.83, 24.12, 25.09, 25.81, 26.31, 26.65, 26.88, 27.05, 27.23, 27.45, 27.77, 28.24, 28.24, 28.24, 28.24],
            //        "marker": {
            //            "enabled": false,
            //            "symbol": null
            //        }
            //    }, {
            //        "name": "25% above ETR",
            //        "type": "line",
            //        "data": [15.50, 24.30, 26.68, 28.33, 29.62, 30.59, 31.31, 31.81, 32.15, 32.38, 32.55, 32.73, 32.95, 33.27, 33.74, 33.74, 33.74, 33.74],
            //        "marker": {
            //            "enabled": false,
            //            "symbol": null
            //        }
            //    }, {
            //        "name": "ETR Score",
            //        "type": "line",
            //        "data": [15.50, 19.10, 21.18, 22.83, 24.12, 25.09, 25.81, 26.31, 26.65, 26.88, 27.05, 27.23, 27.45, 27.77, 28.24, 28.24, 28.24, 28.24],
            //        "marker": {
            //            "enabled": false,
            //            "symbol": null
            //        }
            //    }, {
            //        "name": "25% below ETR",
            //        "type": "line",
            //        "data": [15.50, 13.90, 15.68, 17.33, 18.62, 19.59, 20.31, 20.81, 21.15, 21.38, 21.55, 21.73, 21.95, 22.27, 22.74, 22.74, 22.74, 22.74],
            //        "marker": {
            //            "enabled": false,
            //            "symbol": null
            //        }
            //    }, {
            //        "name": "SRS Cutoff",
            //        "type": "line",
            //        "data": [36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36],
            //        "marker": {
            //            "enabled": false,
            //            "symbol": null
            //        }
            //    }, {
            //        "name": "SRS",
            //        "type": "line",
            //        "data": [33.7, 35.0, 35.0, 36.0, 37.8, 36.0, 32.8, 39.1, null, null, null, null, null, null, null, null, null, null],
            //        "marker": {
            //            "enabled": true,
            //            "symbol": "diamond"
            //        }
            //    }, {
            //        "name": "ORS Cutoff",
            //        "type": "line",
            //        "data": [25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25],
            //        "marker": {
            //            "enabled": false,
            //            "symbol": null
            //        }
            //    }, {
            //        "name": "ORS",
            //        "type": "line",
            //        "data": [15.50, 19.50, 20.00, 21.00, 23.70, 24.00, 23.90, 29.10, 33.00, null, null, null, null, null, null, null, null, null],
            //        "marker": {
            //            "enabled": true,
            //            "symbol": "circle"
            //        }
            //    }],
            //    //"exporting": {

            //    //    /*"buttons": {
            //    //         "printButton": {

            //    //             "enabled": false
            //    //         },
            //    //         "exportButton": {
            //    //             "enabled": true
            //    //         }
            //    //     }*/

            //    //    buttons: {
            //    //        contextButton: {
            //    //            menuItems: [{
            //    //                text: 'Export Chart',
            //    //                onclick: test
            //    //                /*                            function () {
            //    //                                                this.exportChart({}, {
            //    //                                                    title: {
            //    //                                                        text: 'sfasfasfa',
            //    //                                                        style: {
            //    //                                                            width: '450px'
            //    //                                                        }
            //    //                                                    }
            //    //                                                });
            //    //                                            }*/
            //    //            }]
            //    //        }
            //    //    },
            //    //    //,"url": "../Survey/ExportGraph",
            //    //    "filename": "MyFile",
            //    //    "width": 1200,
            //    //    "clientId": 0

            //    //},
            //    "chartType": 1
            //}
                );

        }

        //$("#nightmare").on('click', nightmare);
        //$("#broken").on('click', broken);

    }

});